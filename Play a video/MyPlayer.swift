import UIKit
import MediaPlayer
import AVKit

class MyPlayer: AVPlayerViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
         let url = NSURL(string: "http://jplayer.org/video/m4v/Big_Buck_Bunny_Trailer.m4v")
        //let url = NSURL(string: "http://jplayer.org/video/m4v/Incredibles_Teaser.m4v")
        //let url = NSURL(string: "https://youtu.be/P1FpnRq5A6g")
        // http://tim-prof.cstj.qc.ca/cours/xcode/sources/The.Wizard.Of.Oz.avi
        // let url = NSURL(string: "http://prof-tim.cstj.qc.ca/cours/xcode/sources/The.Wizard.Of.Oz.avi")

        self.player = AVPlayer(url: (url as! URL)) //player
        self.player?.play()
    } // viewDidLoad
} // MyPlayer
