//  Exemple d'un vidéo stream à partir d'une URL
//  Created by Alain on 16-11-25.

import UIKit
import MediaPlayer
import AVKit

class ViewController: UIViewController {

    override func viewDidLoad() {
    let url = NSURL(string: "http://jplayer.org/video/m4v/Big_Buck_Bunny_Trailer.m4v")
    let player = AVPlayer(url: (url as! URL))
    let playerViewController = AVPlayerViewController()
    playerViewController.player = player
    
    playerViewController.view.frame = CGRect(x: 0, y: 0, width: 640, height: 480)
    self.view.addSubview(playerViewController.view)
    self.addChildViewController(playerViewController)
    player.play()
 } // viewDidLoad

} // ViewController

